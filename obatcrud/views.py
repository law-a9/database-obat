import logging

from .models import Obat
from .serializer import ObatSerializer

from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import GenericAPIView, CreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView
from rest_framework.filters import OrderingFilter
from logstash_async.handler import AsynchronousLogstashHandler, LogstashFormatter

logger = logging.getLogger("logstash")
logger.setLevel(logging.DEBUG)

handler = AsynchronousLogstashHandler(
    host='8ef166a3-7883-4013-a052-9bfaf2396397-ls.logit.io',
    port=18893,
    ssl_enable=False,
    database_path='',
)

formatter = LogstashFormatter()
handler.setFormatter(formatter)
logger.addHandler(handler)

class ObatView(GenericAPIView):
    serializer_class = ObatSerializer
    queryset = Obat.objects.all().order_by('id')


class ObatCreateView(ObatView, CreateAPIView):

    def perform_create(self, serializer):
        return serializer.save()


class ObatRetrieveUpdateDeleteView(ObatView, RetrieveUpdateDestroyAPIView):
    lookup_field = "id"

    @method_decorator(cache_page(60))    
    def get(self, request, *args, **kwargs):
        logger.info(f'database-obat: {request.data}')
        return super().get(request, *args, **kwargs)


class ObatListView(ObatView, ListAPIView):
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    ordering_fields = ['name', 'price', 'id']
    ordering = ['name']

    @method_decorator(cache_page(60))
    def get(self, request, *args, **kwargs):
        logger.info(f'database-obat: {request.data}')
        return super().get(request, *args, **kwargs)