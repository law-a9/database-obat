from django.contrib import admin
from .models import Obat, ObatGroup

admin.site.register(Obat)
admin.site.register(ObatGroup)
