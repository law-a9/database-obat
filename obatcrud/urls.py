from obatcrud.views import ObatCreateView
from .views import ObatCreateView, ObatRetrieveUpdateDeleteView, ObatListView
from django.urls import path

app_name = "obatcrud"

urlpatterns = [
    path("", ObatCreateView.as_view(), name="obat-create"),
    path("<int:id>", ObatRetrieveUpdateDeleteView.as_view(), name="obat-retrieveupdatedelete"),
    path("all", ObatListView.as_view(), name="obat-list")
]
