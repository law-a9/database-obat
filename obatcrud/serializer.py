from rest_framework import serializers
from .models import Obat, ObatGroup


class ObatSerializer(serializers.ModelSerializer):
    obat_group = serializers.SlugRelatedField(
        many=False, slug_field='name', allow_null=False, queryset=ObatGroup.objects.all())

    class Meta:
        model = Obat
        fields = "__all__"