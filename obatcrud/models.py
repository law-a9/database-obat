from django.db import models


class ObatGroup(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class Obat(models.Model):
    name = models.CharField(max_length=30)
    price = models.IntegerField()
    description = models.TextField(blank=True, null=True)
    obat_group = models.ForeignKey(ObatGroup, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
